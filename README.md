BIGLottery funding analysis
===========================

The data is the grant data downloaded as CSV from here:

[http://www.biglotteryfund.org.uk/research/open-data](http://www.biglotteryfund.org.uk/research/open-data)

The database is configured using the file `BIGLottery-db-setup.sql` and populated using `BIGLottery-db-upload-all.sql` 


We find 119950 rows of data but linecount gives us 125356 lines with many warnings related to invalid string format, particularly on the Press_summary field.

Changing the Press_Summary field to blob fixes many of the warnings but the blob field can't be read by Tableau. Residual errors seem to arise from malformed entries (i.e. missing quotes) and the occasional oddity with the CURRENT_AWARD field amount which is fixed by converting to char.

BIGGrantOpenData_2006_07.csv is a different column layout to the other 8 files.

BLOB format summary this gives 9 1366 errors on Applicant_name and charity number.

We might want to supplement with data from the census:

[http://www.ons.gov.uk/ons/guide-method/census/2011/census-data/2011-census-prospectus/index.html](http://www.ons.gov.uk/ons/guide-method/census/2011/census-data/2011-census-prospectus/index.html)

We downloaded the census data found here:

[http://www.ons.gov.uk/ons/rel/census/2011-census/key-statistics-and-quick-statistics-for-local-authorities-in-the-united-kingdom---part-1/rft-ks101uk.xls](http://www.ons.gov.uk/ons/rel/census/2011-census/key-statistics-and-quick-statistics-for-local-authorities-in-the-united-kingdom---part-1/rft-ks101uk.xls)

which is linked from this page:

[http://www.ons.gov.uk/ons/publications/re-reference-tables.html?edition=tcm%3A77-327143](http://www.ons.gov.uk/ons/publications/re-reference-tables.html?edition=tcm%3A77-327143)

This needs editing to make it database friendly:
UKCensus2011-KS101-population-by-la.csv

This is uploaded to a database table using the script `BIGLottery-db-census-2011-db-upload.sql`. Values are loaded as chars because they contain commas, and SQL doesn't like commas as thousands separators.

I downloaded local authority polygons for England and Wales in Tableau from here:

[http://tableaumapping.bi/2013/08/03/uk-local-authority-districts/#more-56](http://tableaumapping.bi/2013/08/03/uk-local-authority-districts/#more-56)

These are broken for Northern Ireland, need to fix.

The participation in sport data can be found here:

[http://www.sportengland.org/research/who-plays-sport/by-sport/who-plays-sport/](http://www.sportengland.org/research/who-plays-sport/by-sport/who-plays-sport/)



