#!/usr/bin/env python
# encoding: utf-8

# This is a quick script to correct the mislabelling of the shapefile data for Northern Ireland
# 

#from __future__ import unicode_literals
from collections import OrderedDict
import csv

# First entry is name in tableau extract, second is correct entry
LA_lookup = OrderedDict([
    ('Newry and Mourne', 'Fermanagh'),
    ('Belfast', 'Strabane'),
    ('Castlereagh', 'Omagh'),
    ('Armagh', 'Dungannon'),
    ('Coleraine', 'Armagh'),
    ('Carrickfergus', 'Newry and Mourne'),
    ('Dungannon and South Tyrone', 'Derry'),
    ('Omagh', 'Cookstown'),
    ('Fermanagh', 'Craigavon'),
    ('Craigavon', 'Down'),
    ('Down', 'Limavady'),
    ('Antrim', 'Magherafelt'),
    ('Lisburn', 'Lisburn'),
    ('Limavady', 'Ards'),
    ('Cookstown', 'Castlereagh'),
    ('Ballymena', 'Belfast'),
    ('Ards', 'North Down'),
    ('North Down', 'Newtownabbey'),
    ('Magherafelt', 'Carrickfergus'),
    ('Derry', 'Antrim'),
    ('Banbridge', 'Larne'),
    ('Ballymoney', 'Ballymena'),
    ('Moyle', 'Ballymoney'),
    ('Strabane', 'Coleraine'),
    ('Newtownabbey', 'Moyle'),
    ('Larne', 'Banbridge')
])

inputfile = "C:\\Users\\Ian\\Google Drive\\2013\\2116-BIGLottery\\uk-tableau-mapping-local-authorities-original.csv"
outputfile = "C:\\Users\\Ian\\Google Drive\\2013\\2116-BIGLottery\\uk-tableau-mapping-local-authorities-modified.csv"


input_array = []
passed_areas = set()

with open(inputfile, 'rb') as f:
    reader = csv.reader(f)
    headers = reader.next()
    print headers
    for i,row in enumerate(reader):
        try:
            row[2] = LA_lookup[row[2]]
        except KeyError:
            passed_areas.add(row[2])            

        input_array.append(row)
    f.close()

sep = ','
with open(outputfile, 'wb') as f:
    writer = csv.writer(f, delimiter=sep, quotechar='"')
    writer.writerow(headers)
    for row in input_array:
        writer.writerow(row)
    f.close()

print passed_areas