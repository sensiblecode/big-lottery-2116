USE `BIGLottery2013` ;

DROP TABLE IF EXISTS Census2011KS101;

CREATE TABLE Census2011KS101 (
	Area_code char(200) primary key,
	Area_name char(200),	
	Total_population int,
	Male_population int,
	Female_population int,	
	Households_populations int,	
	Communal_population int,
	Student_home_population int,
	Area_hectares float,
	Density_person_per_hectare float
)
DEFAULT CHARACTER SET utf8
;
LOAD DATA LOCAL INFILE 'C:\\Users\\Ian\\Google Drive\\2013\\2116-BIGLottery\\UKCensus2011-KS101-population-by-la.csv'
INTO TABLE Census2011KS101
FIELDS TERMINATED BY ','
OPTIONALLY ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 LINES
(
	Area_code,
	Area_name,	
	Total_population,
	Male_population,
	Female_population,	
	Households_populations,	
	Communal_population,
	Student_home_population,
	Area_hectares,
	Density_person_per_hectare
	)
